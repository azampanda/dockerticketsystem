<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <div>
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('Tickets') }}
                </h2>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <form method="POST" action="{{ route('tickets.solve', $ticket) }}">
                        @csrf
                        <div>
                            <x-input-label for="title" :value="__('Title')"></x-input-label>
                            <x-text-input id="title" class="block mt-1" type="text" name="title"
                                          :value="$ticket->title" readonly></x-text-input>
                        </div>
                        <div class="mt-4">
                            <x-input-label for="description" :value="__('Description')"></x-input-label>
                            <x-text-input id="description" class="block mt-1" type="text" name="description"
                                          :value="$ticket->description" readonly></x-text-input>
                        </div>
                        <div class="mt-4">
                            <x-input-label for="remarks" :value="__('Remarks')"></x-input-label>
                            <x-text-input id="remarks" class="block mt-1" type="text" name="remarks"
                                          :value="$ticket->remarks ? $ticket->remarks : old('remarks')" required
                                          autofocus></x-text-input>
                        </div>
                        <div class="mt-6">
                            <button
                                class="px-4 py-2 text-white no-underline bg-black rounded hover:bg-gray-600 hover:underline">
                                {{ __('Solve')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
