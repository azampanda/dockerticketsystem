<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <div>
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('Tickets') }}
                </h2>
            </div>

            <div>
                <a class="px-4 py-2 text-white no-underline bg-black rounded hover:bg-gray-600 hover:underline"
                   href="{{route('tickets.create')}}">Create New Ticket</a>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 overflow-x-auto">
                    <h3 class="font-semibold text-l text-gray-800 leading-tight m-2">
                        @if($tickets->count() > 1)
                            {{ __('Your Tickets') }}
                        @else
                            {{ __('Your Ticket') }}
                        @endif
                    </h3>
                    <table class="min-w-full leading-normal">
                        <thead>
                        <tr>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Date Created
                            </th>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Title
                            </th>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Description
                            </th>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Remarks
                            </th>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Status
                            </th>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($tickets->count() == 0)
                            <tr>
                                <td colspan="5" class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                    <center>No tickets found</center>
                                </td>
                            </tr>
                        @else
                            @foreach( $tickets as $ticket )
                                <tr>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <p class="text-gray-900 whitespace-no-wrap">
                                            {{ $ticket->created_at }}
                                        </p>
                                    </td>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <p class="text-gray-900 whitespace-no-wrap">
                                            {{ $ticket->title }}
                                        </p>
                                    </td>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <p class="text-gray-900 whitespace-no-wrap">
                                            {{ $ticket->description }}
                                        </p>
                                    </td>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <p class="text-gray-900 whitespace-no-wrap">
                                            {{ $ticket->remarks }}
                                        </p>
                                    </td>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <p class="whitespace-no-wrap {{ $ticket->status ? 'text-red-500' : 'text-green-500' }}">
                                            {{ $ticket->status ? 'CLOSED' : 'OPEN' }}
                                        </p>
                                    </td>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <div class="flex m-auto justify-start">
                                            <form id="deleteTicket" method="post"
                                                  action="{{ route('tickets.destroy', $ticket) }}">
                                                @csrf
                                                @method('delete')
                                            </form>
                                            <a
                                                href="{{ route('tickets.edit', $ticket) }}"
                                                class="ml-2 px-4 py-2 text-white no-underline bg-gray-600 rounded hover:bg-gray-600 hover:underline">
                                                Info
                                            </a>
                                            <button
                                                form="deleteTicket"
                                                class="ml-1 px-4 py-2 text-white no-underline bg-red-600 rounded hover:bg-gray-600 hover:underline"
                                                onclick="confirm('Are you sure?');"
                                            >
                                                Delete
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <div class="mt-4">
                        {{ $tickets->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 overflow-x-auto">
                    <h3 class="font-semibold text-l text-gray-800 leading-tight m-2">
                        @if($tickets->count() > 1)
                            {{ __('Assigned Tickets') }}
                        @else
                            {{ __('Assigned Ticket') }}
                        @endif
                    </h3>
                    <table class="min-w-full leading-normal">
                        <thead>
                        <tr>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Date Created
                            </th>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Title
                            </th>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Description
                            </th>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Remarks
                            </th>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Status
                            </th>
                            <th class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($ticketAssignments->count() == 0)
                            <tr>
                                <td colspan="5" class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                    <center>No tickets found</center>
                                </td>
                            </tr>
                        @else
                            @foreach( $ticketAssignments as $ticketAssignment )
                                <tr>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <p class="text-gray-900 whitespace-no-wrap">
                                            {{ $ticketAssignment->ticket->created_at }}
                                        </p>
                                    </td>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <p class="text-gray-900 whitespace-no-wrap">
                                            {{ $ticketAssignment->ticket->title }}
                                        </p>
                                    </td>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <p class="text-gray-900 whitespace-no-wrap">
                                            {{ $ticketAssignment->ticket->description }}
                                        </p>
                                    </td>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <p class="text-gray-900 whitespace-no-wrap">
                                            {{ $ticketAssignment->ticket->remarks }}
                                        </p>
                                    </td>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <p class="whitespace-no-wrap {{ $ticketAssignment->ticket->status ? 'text-red-500' : 'text-green-500' }}">
                                            {{ $ticketAssignment->ticket->status ? 'CLOSED' : 'OPEN' }}
                                        </p>
                                    </td>
                                    <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                        <div class="flex m-auto justify-start">
                                            <a
                                                href="{{ route('tickets.resolve', $ticketAssignment->ticket) }}"
                                                class="px-4 py-2 text-white no-underline bg-blue-400 rounded hover:bg-gray-600 hover:underline">
                                                Resolve
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <div class="mt-4">
                        {{ $ticketAssignments->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
