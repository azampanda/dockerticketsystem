<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <div>
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('Tickets') }}
                </h2>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <form method="POST"
                          action="{{ $isUpdate ? route('tickets.update', $ticket) : route('tickets.store') }}">
                        @csrf
                        <div>
                            <x-input-label for="title" :value="__('Title')"></x-input-label>
                            <x-text-input id="title" class="block mt-1" type="text" name="title"
                                          :value="$isUpdate ? $ticket->title : old('title')" required
                                          autofocus></x-text-input>
                        </div>
                        <div class="mt-4">
                            <x-input-label for="description" :value="__('Description')"></x-input-label>
                            <x-text-input id="description" class="block mt-1" type="text" name="description"
                                          :value="$isUpdate ? $ticket->description : old('description')"
                                          required></x-text-input>
                        </div>
                        @if($isUpdate)
                            <div class="mt-4">
                                <x-input-label for="assignment" :value="__('Assign to')"></x-input-label>
                                <select id="assignment" name="assignment">
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}">{{  $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="mt-6">
                            <button
                                class="px-4 py-2 text-white no-underline bg-black rounded hover:bg-gray-600 hover:underline">
                                {{ $isUpdate ? __('Update') : __('Create')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
