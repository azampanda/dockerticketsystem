<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TicketController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/tickets', [TicketController::class, 'index'])->name('tickets.index');
    Route::get('/tickets/create', [TicketController::class, 'create'])->name('tickets.create');
    Route::post('/tickets/store', [TicketController::class, 'store'])->name('tickets.store');
    Route::get('/tickets/edit/{ticket}', [TicketController::class, 'edit'])->name('tickets.edit');
    Route::post('/tickets/update/{ticket}', [TicketController::class, 'update'])->name('tickets.update');
    Route::get('/tickets/resolve/{ticket}', [TicketController::class, 'resolve'])->name('tickets.resolve');
    Route::post('/tickets/solve/{ticket}', [TicketController::class, 'solve'])->name('tickets.solve');
    Route::delete('/tickets/delete/{ticket}', [TicketController::class, 'destroy'])->name('tickets.destroy');
});

require __DIR__ . '/auth.php';
