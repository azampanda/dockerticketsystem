<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use App\Models\TicketAssignment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    public function index()
    {
        $tickets = Ticket::where('user_id', Auth::user()->id)->paginate(20);
        $ticketAssignments = Auth::user()->TicketAssignments()->with('Ticket')->paginate(20);

        return view('tickets.index', compact('tickets', 'ticketAssignments'));
    }

    public function create()
    {
        $isUpdate = false;
        $users = User::where('id', '!=', Auth::user()->id)->get();

        return view('tickets.form', compact('isUpdate', 'users'));
    }

    public function edit(Ticket $ticket)
    {
        $isUpdate = true;
        $users = User::where('id', '!=', Auth::user()->id)->get();

        return view('tickets.form', compact('isUpdate', 'ticket', 'users'));
    }

    public function resolve(Ticket $ticket)
    {
        return view('tickets.resolve', compact('ticket'));
    }

    public function solve(Request $request, Ticket $ticket)
    {
        $request->validate([
            'remarks' => ['required', 'string']
        ]);

        $ticket->remarks = $request->remarks;
        $ticket->status = true;
        $ticket->save();

        return redirect('tickets');
    }

    public function update(Request $request, Ticket $ticket)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
        ]);

        $ticket->title = $request->title;
        $ticket->description = $request->description;
        $ticket->save();

        $ticketAssignment = TicketAssignment::where('user_id', Auth::user()->id)
            ->where('ticket_id', $ticket->id)
            ->first();
        if ($ticketAssignment) {
            $ticketAssignment->user_id = $request->assignment;
            $ticketAssignment->save();
        } else {
            $ticketAssignment = new TicketAssignment();
            $ticketAssignment->user_id = $request->assignment;
            $ticketAssignment->ticket_id = $ticket->id;
            $ticketAssignment->save();
        }


        return redirect('tickets');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
        ]);

        $ticket = new Ticket();
        $ticket->title = $request->title;
        $ticket->description = $request->description;
        $ticket->user_id = Auth::user()->id;
        $ticket->save();

        return redirect('tickets');
    }

    public function destroy(Ticket $ticket)
    {
        $ticket->delete();

        return redirect('tickets');
    }
}
