<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $attributes = [
        'status' => false,
    ];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function TicketAssignments()
    {
        return $this->hasMany(TicketAssignment::class, 'ticket_id');
    }
}
