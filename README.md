## DockerTicketSystem
A simple crud app (maintenance ticketing system) that allows a user (a) to log a report of an issue, generate a ticket and assign it to another user (b). User (b) would then be able to update the ticket status with remarks.

## Requirements
- PHP v8.1 and above
- Composer v2.4 and above
- Node v16 and above
- NPM v8.19 and above

## Installation
1. Install dependencies
```
$ composer install && npm install
```
2. Copy Env file from `.env.example`
```
$ cp .env.example .env
```
3. Create an empty database

4. Replace the values in `.env` with your database credentials

5. Generate App Key
```
$ php artisan key:generate
```
6. Run Front-End HMR
```
$ npm run dev
```
7. Run Migration
```
$ php artisan migrate --seed
```
8. In new Terminal/Console, serve the site
```
$ php artisan serve
```

## Docker
