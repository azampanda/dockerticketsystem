#!/bin/bash

cp .env.example .env
composer install && npm install
php artisan key:generate
echo "yes" | php artisan migrate --seed
npm run build
