<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultPassword = 'password';

        User::create([
            'name' => "First User",
            'email' => 'user1@test.com',
            'password' => Hash::make($defaultPassword),
            'email_verified_at' => now(),
        ]);

        User::create([
            'name' => "Second User",
            'email' => 'user2@test.com',
            'password' => Hash::make($defaultPassword),
            'email_verified_at' => now(),
        ]);

        User::create([
            'name' => "Third User",
            'email' => 'user3@test.com',
            'password' => Hash::make($defaultPassword),
            'email_verified_at' => now(),
        ]);
    }
}
